var items = document.querySelectorAll('.platform_list li');
var targets = [];
var selected = 'pc';
var categories = ['rifle', 'submachine', 'lightmachine', 'marksman', 'handgun', 'shotgun', 'machine'];
var seasonalStats = ['kills', 'deaths', 'wins', 'losses', 'abandons', 'mmr', 'max_mmr'];
var queueTypes = ['ranked', 'casual', 'other'];
var queueStats = ['kills', 'deaths', 'games_played', 'wins', 'losses'];
var queueDoubles = ['kd', 'wl'];
var generalStats = ['games_played', 'wins', 'losses', 'kills', 'deaths', 'blind_kills', 'assists',
  'dbnos', 'headshots', 'melee_kills', 'penetration_kills', 'revives', 'suicides'];
var generalDoubles = ['kd', 'wl'];

document.getElementById('stats').style.display = 'none';
document.getElementById('error').style.display = 'none';

[].forEach.call(items, function(item) {
    var text = item.textContent.trim().toLowerCase();
    targets.push(item);
    item.addEventListener('click', function() {
        targets.forEach(x => {
            x.setAttribute('class', 'platform_platform platform_inactive');
        });
        const select = targets.find(elements => elements.textContent.toLowerCase() == text);
        selected = select.textContent.toLowerCase();
        select.setAttribute('class', 'platform_platform platform_active');
    });
});

document.getElementById('back').addEventListener('click', function() {
  document.getElementById('stats').style.display = 'none';
  document.getElementById('searchbox').style.display = 'flex';
  document.getElementById('error').style.display = 'none';
})

document.getElementById('search').addEventListener('click', function() {
    var input = document.getElementById('name').value;

    $.ajax({
    url: `https://api2.r6stats.com/public-api/stats/${input}/${selected}/generic`,
      type: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer d64fe5b9-a6b9-478f-a7a9-262939ad2ee2'
      },
      success: function(response) {
        document.getElementById('searchbox').style.display = 'none';
        document.getElementById('stats').style.display = 'flex';
        document.getElementById('error').style.display = 'none';

        document.getElementById('nametag').textContent = response.username;
        document.getElementById('profilepic').setAttribute('src', response.avatar_url_146);
        document.getElementById('level').textContent = response.progression.level;
        document.getElementById('lootbox').textContent = response.progression.lootbox_probability + '%';
        document.getElementById('playtime').textContent = Math.round((response.stats.general.playtime) / 3600) + 'hrs';

        generalDoubles.forEach(stat => {
          document.getElementById(stat).textContent = Math.round((response.stats.general[stat] + Number.EPSILON) * 100) / 100;
        });

        generalStats.forEach(stat => {
          document.getElementById(stat).textContent = response.stats.general[stat];
        });

        queueTypes.forEach(queue => {
          queueStats.forEach(stat => {
            document.getElementById(`${queue}${stat}`).textContent = response.stats.queue[queue][stat];
          });
          queueDoubles.forEach(stat => {
            document.getElementById(`${queue}${stat}`).textContent = Math.round((response.stats.queue[queue][stat] + Number.EPSILON) * 100) / 100;
          });
        });
      },
      error: function(response) {
        document.getElementById('error').style.display = 'flex';
      }
  });

  $.ajax({
    url: `https://api2.r6stats.com/public-api/stats/${input}/${selected}/seasonal`,
      type: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer d64fe5b9-a6b9-478f-a7a9-262939ad2ee2'
      },
      success: function(response) {
        console.log(response);
        document.getElementById('startdate').textContent = new Date(response.seasons.crimson_heist.start_date).toLocaleDateString();

        seasonalStats.forEach(stat => {
          document.getElementById(`seasonal${stat}`).textContent = response.seasons.crimson_heist.regions.emea[0][stat];
        })

        document.getElementById('rankpic').setAttribute('src', response.seasons.crimson_heist.regions.emea[0].rank_image);
        document.getElementById('rankpic').setAttribute('title', `Aktueller Rang: ${response.seasons.crimson_heist.regions.emea[0].rank_text}`);
        document.getElementById('mrankpic').setAttribute('src', response.seasons.crimson_heist.regions.emea[0].max_rank_image);
        document.getElementById('mrankpic').setAttribute('title', `Höchster Rang: ${response.seasons.crimson_heist.regions.emea[0].max_rank_text}`);

        var result = response.seasons.crimson_heist.regions.emea[0].last_match_result;
        document.getElementById('matchres').textContent = result == 1 ? 'Sieg' : (result == 2 ? 'Niederlage' : 'Unbekannt');

        let mmrImpact = response.seasons.crimson_heist.regions.emea[0].last_match_mmr_change;
        document.getElementById('matchimpact').textContent = mmrImpact;
        document.getElementById('matchimpact').style.color = mmrImpact >= 0 ? '#27a847' : '#c43535';
        
      },
      error: function(response)
      {
        console.log('Error');
      }
  });
  $.ajax({
    url: `https://api2.r6stats.com/public-api/stats/${input}/${selected}/weapon-categories`,
      type: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer d64fe5b9-a6b9-478f-a7a9-262939ad2ee2'
      },
      success: function(response) {
        for (var i = 0; i < categories.length; i++)
        {
          var category = categories[i];
          document.getElementById(`${category}kills`).textContent = response.categories[i].kills;
          document.getElementById(`${category}kdr`).textContent = Math.round((response.categories[i].kd + Number.EPSILON) * 100) / 100;
          document.getElementById(`${category}headshots`).textContent = Math.round((response.categories[i].headshot_percentage + Number.EPSILON) * 100) + '%';
          document.getElementById(`${category}picked`).textContent = response.categories[i].times_chosen + "x";
        }
      }
  });
});

scrollButton = document.getElementById('returnToTop');

window.onscroll = function() { scrollFunction() };

function scrollFunction() 
{
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) 
  {
    scrollButton.style.display = "block";
  } 
  else 
  {
    scrollButton.style.display = "none";
  }
}

scrollButton.addEventListener('click', function() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
});
